<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

class GoogleAPIManager {

    private static ?GoogleAPIManager $_instance = null;


    public static function getInstance(): GoogleApiManager {
        if (is_null(self::$_instance)) {
            self::$_instance = new GoogleAPIManager();
        }

        return self::$_instance;
    }

    private Google_Client $client;

    private function __construct() {
        $this->client = new Google_Client();
        $this->client->setApplicationName('Google Calendar API PHP Quickstart');
        $this->client->setScopes(Google_Service_Calendar::CALENDAR);
        $this->client->setAuthConfig($_SERVER['DOCUMENT_ROOT'] . '/credentials.json');
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = $_SERVER['DOCUMENT_ROOT'] . '/token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $this->client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($this->client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($this->client->getRefreshToken()) {
                $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $this->client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
                $this->client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($this->client->getAccessToken()));
        }
    }

    private function generateService(): Google_Service_Calendar {
        return new Google_Service_Calendar($this->client);
    }

    public function createEvent(Google_Service_Calendar_Event $event, string $calID = "primary"): Google_Service_Calendar_Event {
        return $this->generateService()->events->insert($calID, $event, array(
            "sendUpdates" => "all"
        ));
    }

    public function listEvents(): Google_Service_Calendar_Events {
        return $this->generateService()->events->listEvents("primary");
    }

    public function getEvent(string $eventID, string $calID = "primary"): ?Google_Service_Calendar_Event {
        try {
            return $this->generateService()->events->get($calID, $eventID);
        } catch (Exception $e) {
            return null;
        }
    }

    public function deleteEvent(string $eventID, string $calID = "primary") {
        $this->generateService()->events->delete($calID, $eventID, array(
            "sendUpdates" => "all"
        ));
    }

    public function updateEvent(Google_Service_Calendar_Event $event, string $calID = "primary"): ?Google_Service_Calendar_Event {
        try {
            return $this->generateService()->events->update($calID, $event->getId(), $event, array(
                "sendUpdates" => "all"
            ));
        } catch (Exception $e) {
            return null;
        }
    }

    public function generateAttendeesArray($attendees): array {
        function generateAttendee($attendee): array {
            return ["email" => $attendee];
        }

        return array_map('generateAttendee', $attendees);
    }

}

?>