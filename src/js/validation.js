/**
 * Permet de renvoyer un nombre aleatoire
 * @param max
 * @returns {number}
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}


/**
 * Permet de generer un captcha aleatoirment a chaque chargement de page au format XXX000
 * @param mdp
 * @returns {string}
 */
function mdpauto(mdp) {
    var new_capt = "";
    var r = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (i = 0; i < 3; i++) {
        num = r.charAt(getRandomInt(26));
        new_capt = new_capt + num;
    }
    for (i = 0; i < 3; i++) {
        num = getRandomInt(10);
        new_capt = new_capt + num;
    }
    mdp.value = new_capt
    return new_capt;

}

function test(){
    document.getElementById("eye").addEventListener("click", function(e){
        var pwd = document.getElementById("pass");
        if(pwd.getAttribute("type")=="password"){
            pwd.setAttribute("type","text");
        } else {
            pwd.setAttribute("type","password");
        }
    });
}


