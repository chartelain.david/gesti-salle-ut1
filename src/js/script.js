/**
 * cache du code HTML
 */
function hide() {
    document.getElementById("popup").style.visibility = "hidden"
    document.getElementById("dim").style.visibility = "hidden"
    document.getElementById("participant-add").style.visibility = "hidden"
}

/**
 * rend visible ou cache du code HTML selon le mode
 * @param { String } mode
 */
function reveal(mode) {
    document.getElementById("popup").style.visibility = "visible"
    document.getElementById("dim").style.visibility = "visible"
    document.getElementById("modification-popup").style.display = (mode === "editer") ? "block" : "none"
    document.getElementById("add-popup").style.display = (mode === "ajouter") ? "block" : "none"
}

/**
 *
 * @param { number } id_reunion
 * @param { string[] } attendeesListID
 * @param { string } eventID
 */
function revealMail(id_reunion, attendeesListID, eventID) {
    const popupAttendees = document.getElementById("participant-add")
    axios.get("/application/requests/reunion.php?reunion_id=" + id_reunion)
        .then(function (response) {
            const attendeesList = document.getElementById("participant-add-list")

            let child = attendeesList.firstChild
            while (child != null) {
                attendeesList.removeChild(child)
                child = attendeesList.firstChild
            }

            response.data.forEach(function (email) {
                ajouterParticipant(email, document.getElementById(attendeesListID))
                // const mail = document.createElement('P')
                // mail.innerText = email
                // attendeesList.appendChild(mail)
            })

            document.getElementById("form-reunion-id").value = id_reunion
            document.getElementById("form-eventId").value = eventID

            popupAttendees.style.visibility = "visible"
            document.getElementById("dim").style.visibility = "visible"

        })
        .catch(function(error) {
            console.error(error)
            alert("Impossible de récupérer la réunion")
        })
}

/**
 *Attribue la valeur correspondante de la bdd à l'input associé
 * @param { any } data
 */
function openPopupWith(data) {
    data.idUtilisateur
    document.getElementById("email").value = data.email;
    document.getElementById("idUtilisateur").value = data.idUtilisateur;
    document.getElementById("statut").value = data.statut;
    document.getElementById("password").value = data.password;

    reveal('edit')
}

/**
 *Attribue la valeur correspondante de la bdd à l'input associé
 * @param { any } data
 */
function openPopupWith2(data) {
    data.numsalle
    document.getElementById("numsalle").value = data.numsalle;
    document.getElementById("libelle").value = data.libelle;
    document.getElementById("capacite").value = data.capacite;
    document.getElementById("equipementvisuel").value = data.equipementvisuel;
    document.getElementById("equipementaudio").value = data.equipementaudio;

    reveal('edit')
}

/**
 *Attribue la valeur correspondante de la bdd à l'input associé
 * @param { any } data
 */
function openPopupEvent(data) {
    data.id_reunion
    document.getElementById("reunionnom").value = data.reunionnom;
    document.getElementById("reuniondate").value = data.reuniondate;
    document.getElementById("heuredebut").value= data.heuredebut;
    document.getElementById("reunionduree").value = data.reunionduree;
    document.getElementById("description").value = data.description;
    document.getElementById("nbparticipant").value = data.nbparticipant;
    document.getElementById("id_reunion").value = data.id_reunion;
    document.getElementById("eventId").value = data.eventId;
}
