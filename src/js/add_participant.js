/**
 * Met en forme et ajoute dans le code HTML le/les adresse(s) mail
 * @param { string } email
 * @param { string[] } listeParticipants
 */
function ajouterParticipant(email, listeParticipants) {
    const conteneur = document.createElement('div');
    conteneur.classList.add('email-style');

    let valeur = document.createElement('span');
    valeur = email.trim();

    const remove = document.createElement('span');
    remove.classList.add('clickable')
    const iconRemove = document.createElement('i');
    iconRemove.classList.add('far', 'fa-times-circle');
    remove.appendChild(iconRemove);
    remove.onclick = () => {
        listeParticipants.removeChild(conteneur);
    }

    const nouveauTexte = document.createElement('input');
    nouveauTexte.setAttribute('type', 'hidden');
    nouveauTexte.value = email.trim();
    nouveauTexte.name = 'participants[]'

    conteneur.append(valeur, remove, nouveauTexte);

    listeParticipants.appendChild(conteneur);
}

/**
 * Vérifie la saisie et appel la fonction ajouterParticipant pour ajouter le/les adresse(s) mail
 * @param { string } event
 * @param { string }listID
 */
function ajouterSiNecessaire(event, listID) {
    const emails = event.target.value;
    if(emails.trim() === "" || emails.indexOf(";") === -1){
        return
    }

    const list = document.getElementById(listID);
    emails.split(";").forEach(email => {
        const emailTrimmed = email.trim();
        if (emailTrimmed === "") {
            return
        }

        ajouterParticipant(emailTrimmed,list);
    })

    event.target.value = "";
}

// function addParticipant(tableID) {
//     let chaine = document.getElementById("email").value;
//     let refTable = document.getElementById(tableID);
//     console.log("vous avez saisi : " + chaine);
//     let tableau = chaine.split(";");
//     console.log(tableau);
//     const listeParticipants = document.getElementById(tableID);
//
//     for (const email of tableau) {
//         ajouterParticipant(email, listeParticipants);
//     }
// }