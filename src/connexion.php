<?php

$conn = mysqli_connect(
    $_ENV["DB_URL"],
    $_ENV["DB_USER"],
    $_ENV["DB_PASSWORD"],
    $_ENV["DB_NAME"]
);

if(!$conn) {
    echo "La connexion au serveur a échoué !<br>";
    echo "Configuration utilisée pour la connexion :<br>";
    echo 'url:' . $_ENV['DB_URL'] . '<br>';
    echo 'user:' . $_ENV['DB_USER'] . '<br>';
    echo 'password:' . $_ENV['DB_PASSWORD'] . '<br>';
    echo 'db:' . $_ENV['DB_NAME'] . '<br>';
    exit();
}

$conn->set_charset("utf8");

?>