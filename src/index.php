<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="/css/style.css">

    <?php include_once $_SERVER['DOCUMENT_ROOT'] . "/application/common/libraries.php" ?>
    <script language="JavaScript" type="text/javascript" src="/js/validation.js"></script>
</head>

<body>
    <img src="/res/connexionImage.jpg" alt="" id="connexionimage">
    <form action="/application/authentication/login.php" method="post">
        <a href="https://www.ut-capitole.fr/" target="_blank"><img src="/res/logo/Université_Toulouse_1_(logo).png" class="logo_fac" alt=""></a>
        <h1 class="title">Connexion</h1>
        <div class="cont">
            <div class="style-animation">
                <input type="text" name="email" class="input-style-animated" required />
                <label for="nom">Adresse mail</label>
            </div>
            <div class="style-animation">
                <input type="password" class="input-style-animated" name="password" required />
                <label for="mdp">Mot de passe</label>
            </div>
        </div>
        <input onclick="" type="submit" value="Se connecter" class="button" name="btnLogin">
        <div class="social-network">
            <a href="https://www.facebook.com/ut1capitole/" target="_blank"><i class="fab fa-facebook-square"></i></a>
            <a href="https://twitter.com/UT1Capitole" target="_blank"><i class="fab fa-twitter-square"></i></a>
            <a href="https://www.instagram.com/ut1capitole/" target="_blank"><i class="fab fa-instagram"></i></a>
        </div>
    </form>
</body>

</html>
