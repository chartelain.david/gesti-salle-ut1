<?php
session_start();

require_once "debug.php";

require "debug.php";
require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';



$search = "";
$page = 0;
if (isset($_GET['search'])) {
    $search = htmlspecialchars($_GET['search']);
}

if (isset($_GET['page'])) {
    $page = $_GET['page'] - 1;
}

if ($page < 0) {
    $page = 0;
}

$page_size = 20;
$start = $page * $page_size;

/**
 * Gere la liste des reunions par rapport au statut de l'utilisateur
 * @param string $search
 * @param int $page_size
 * @param int $start
 * @return mixed
 */
function getRequestsGivenUserStatus($search, $page_size, $start) {
    global $conn;
    switch ($_SESSION['status']) {
        case 'superutilisateur' :
            return [
                "request" => "SELECT * FROM reunion r WHERE r.reunionnom LIKE '%$search%' LIMIT $page_size OFFSET $start",
                "count" => "SELECT count(1) FROM reunion r WHERE r.reunionnom LIKE '%$search%'"
            ];


        default:
            $emailSession = $_SESSION['email'];
            $user_request = "SELECT id_utilisateur FROM utilisateur WHERE email = '$emailSession' ";
            $result = mysqli_query($conn, $user_request);
            while($data1 = $result->fetch_row()) {
                $util = $data1[0];
            }

            return [
                "request" => "SELECT * FROM reunion r, utilisateur u WHERE r.id_utilisateur = $util AND  u.id_utilisateur= $util AND r.reunionnom LIKE '%$search%' LIMIT $page_size OFFSET $start",
                "count" => "SELECT count(1) FROM reunion r WHERE r.reunionnom LIKE '%$search%' AND r.id_utilisateur = $util"
            ];
    }
}

["request" => $req, "count" => $req_count] = getRequestsGivenUserStatus($search, $page_size, $start);

$recherche_count = mysqli_query($conn, $req_count);
$count = ceil($recherche_count->fetch_row()[0] / $page_size);
$recherche_count->close();

$recherche = mysqli_query($conn, $req);
?>


<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Mes réunions</title>
    <link rel="stylesheet" href="/css/gestionUtilisateurs.css">
    <link rel="stylesheet" href="/css/listeReunion.css">
    <script rel="script" src="/js/script.js"></script>
    <script rel="script" src="/js/add_participant.js"></script>
    <script rel="script" src="/js/validation.js"></script>

    <?php include "common/libraries.php" ?>
</head>
<body>

<?php include "common/navigation.php" ?>

<div class="content">
    <div class="search-area">
        <form method="GET">
            <input type="text" placeholder="Entrer un nom de réunion" name="search" class="search-value" id="search"
                   value="<?php echo $search ?>">
            <input type="hidden" value="1" name="page">
            <input class="button-style-2 clickable" type="submit" value="rechercher">
        </form>
        <a href="accueil.php">
            <button class="add-button button-style-2">
                <img src="/res/icon/add.svg">
            </button>
        </a>
    </div>
    <hr>

    <table>
        <thead class="">
        <tr>
            <th>Date réunion</th>
            <th>Nom réunion</th>
            <th>Heure de début</th>
            <th>Durée réunion</th>
            <th>Récurrence</th>
            <th>Nbr de recurrence</th>
            <th>Description</th>
            <th>Nombre participant</th>
            <th>Numéro de la salle</th>
        </tr>
        </thead>
        <tbody id="tableBody">
        <?php

        /// Tant quil reste des lignes à afficher...
        while ($data = $recherche->fetch_row()) {
            /// ...on ajoute une ligne au tableau,...
            ?>
            <tr>
                <?php
                /// ...pour chaque colonne (de la ligne)...
                for ($k = 1; $k <= 9; $k++) {
                    /// ...On affiche l'information correspondante
                    echo "<td>$data[$k]</td>";
                }

                $data_object = json_encode(array(
                    "id_reunion" => $data[0],
                    "reuniondate" => $data[1],
                    "reunionnom" => $data[2],
                    "heuredebut" => $data[3],
                    "reunionduree" => $data[4],
                    "recurrence" => $data[5],
                    "freqreccu" => $data[6],
                    "description" => $data[7],
                    "nbparticipant" => $data[8],
                    "num_salle" => $data[9],
                    "id_utilisateur" => $data[10],
                    "eventId" => $data[11]
                ));
                /// Bouton qui permet de modifier un contact
                /// Bouton qui permet de supprimer un contact
                echo "
                        <td>
                                <a class='link-icon' onclick='revealMail(\"$data[0]\", \"participant-add-list\", \"$data[11]\")'>
                                    <img src='/res/icon/people.svg'>
                                </a>
                                <img src='/res/icon/edit.svg' onclick='openPopupEvent($data_object); reveal(\"editer\")' class='link-icon'> 
                                <a href='/application/requests/reqsuppressionReunion.php?id_reunion=$data[0]' class='link-icon' onclick='return confirm(\"Etes-vous sur de votre choix ?\");'>
                                    <img src='/res/icon/delete.svg'>
                                </a>
                       </td>
                        ";
                /// Fin de la ligne

                ?>
            </tr>
            <?php
        }
        ?>
        <! Fin du corps du tableau !>
        </tbody>
        <! Fin du tableau !>
    </table>
    
    <div class="area-page">
        <?php
        /**
* Gere la barre de recherche et la pagination
*/
        $reference_page = $page + 1;
        $next_page = $reference_page + 1;
        $return_page = $reference_page - 1;

        echo "page $reference_page/";
        if ($count == 0) {
            echo 1;
        } else {
            echo $count;
        }

        echo "<div>";
        if ($return_page > 0) {
            echo "
                            <a href='liste-reunion.php?page=$return_page&search=$search'>
                                <button class='clickable button-style-2'>Retour</button>
                            </a>
                            ";
        }

        if ($next_page <= $count) {
            echo "
                            <a href='liste-reunion.php?page=$next_page&search=$search'>
                                <button class='clickable button-style-2'>Suivant</button>
                            </a>
                            ";
        }
        echo "</div>";
        ?>
    </div>
</div>
<!-- Formulaire de modification de reunion en forme de pop up-->
<div id="dim" onclick="hide()"></div>
<div id="popup">
    <div id="modification-popup" class="content-popup">
        <form action="salle-apres-modif.php" method="GET">
            <div>
                Nom de la reunion : <input type="text" name="reunionnom" id="reunionnom" required>
            </div>
            <div>
                Date de la reunion : <input type="date" name="reuniondate" id="reuniondate" required>
            </div>
            <div>
                Heure de début de réunion : <input type="datetime-local" name="heuredebut" id="heuredebut" required>
            </div>
            <div>
                <label for="">Durée de la réunion : </label>
                <select id="reunionduree" name="reunionduree" required>
                    <option value="00:30:00">30min</option>
                    <option value="00:45:00">45min</option>
                    <option value="01:00:00">1h</option>
                    <option value="01:30:00">1h30min</option>
                    <option value="02:00:00">2h</option>
                    <option value="02:30:00">2h30min</option>
                    <option value="03:00:00">3h</option>
                    <option value="03:30:00">3h30min</option>
                    <option value="04:00:00">4h</option>
                    <option value="04:30:00">4h30min</option>
                    <option value="05:00:00">5h</option>
                    <option value="05:30:00">5h30min</option>
                    <option value="06:00:00">6h</option>
                    <option value="06:30:00">6h30min</option>
                    <option value="07:00:00">7h</option>
                    <option value="07:30:00">7h30min</option>
                    <option value="08:00:00">8h</option>
                    <option value="08:30:00">8h30min</option>
                    <option value="09:00:00">9h</option>
                    <option value="09:30:00">9h30min</option>
                    <option value="10:00:00">10h</option>
                    <option value="10:30:00">10h30min</option>
                    <option value="11:00:00">11h</option>
                    <option value="11:30:00">11h30min</option>
                    <option value="12:00:00">12h</option>
                    <option value="12:30:00">12h30min</option>
                    <option value="13:00:00">13h</option>
                    <option value="13:30:00">13h30min</option>
                    <option value="14:00:00">14h</option>
                    <option value="14:30:00">14h30min</option>
                    <option value="15:00:00">15h</option>
                    <option value="15:30:00">15h30min</option>
                    <option value="16:00:00">16h</option>
                </select>
            </div>
            <div>
                Description : <input type="text" name="description" id="description">
            </div>
            <input type="hidden" id="nbparticipant" name="nbparticipant">
            <input type="hidden" id="id_reunion" name="id_reunion">
            <input type="hidden" id="eventId" name="eventId">
            <div class="button-area">
                <input type="submit" name="modif" value="Modifier">
            </div>
        </form>
    </div>
</div>
<!-- Formulaire d'ajout et de modification de participant en forme de pop up-->
<div id="participant-add" class="attendees-popup">
    <form method="post" action="/application/requests/reunion.php">
        <div class="espace">
            <h2>Modification des participants</h2>
            <i class="small-text">
                L'adresse mail saisie doit finir par un point-virgule pour être validée ex :"lorem@gmail.com;".
                <br>
                <br>
                Pour ajouter une liste d'e-mails par un copier-coller, veillez à ce que chaque mail finisse par un
                point-virgule ex :"lorem@gmail.com; ipsum@gmail.com; dolor@gmail.com;".
            </i>
        </div>
        <div class="style-animation">
            <input type="text" id="email" class="input-style-animated">
            <label for="email">Adresse mail</label>
        </div>

        <div id="participant-add-list" class="attendees-list scrollable"></div>
        <input type="hidden" id="form-reunion-id" name="reunion_id">
        <input type="hidden" id="form-eventId" name="eventId">
        <input type="submit" class="button-style-1 clickable" value="Modifier">
    </form>
</div>

<script>
    document.getElementById('email').oninput = (event) => {
        console.log(event.target.value);
        ajouterSiNecessaire(event, "participant-add-list");
    }
</script>
</body>
</html>
