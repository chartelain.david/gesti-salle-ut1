<?php
session_start();

require_once "debug.php";

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
include_once "authentication/check-login.php";

$emailSession = $_SESSION['email'];

$DateR = $_GET['DateR'];
$NomEv = $_GET['NomEv'];
$DureeR = $_GET['DureeR'];
$Hdebut = $_GET['Hdebut'];
$nbParticipant = $_GET['nbParticipant'];
$Description = $_GET['Description'];
$recurrence = $_GET['recurrence'];
$freqreccu = $_GET['freqreccu'];
$equipementA = $_GET['equipaudio'];
$equipementV = $_GET['equipvideo'];


$req = "SELECT id_utilisateur FROM utilisateur WHERE email = '$emailSession' ";
$id_utilisateur = mysqli_query($conn, $req);


$date = new DateTime($DateR);
$time = new DateTime($Hdebut);
$time2 = new DateTime($DureeR);


$date->setTime($time->format('H'), $time->format('i'), $time->format('s'));
$dtimedebut = $date->format('Y-m-d H:i:s'); // Outputs '2017-03-14 13:37:42'
$dtime2 = $time2->format('H:i:s');

$page = 0;

if (isset($_GET['page'])) {
    $page = $_GET['page'] - 1;
}

if ($page < 0) {
    $page = 0;
}

///Requête du filtrage selon les equipements videos et audios mais aussi la capacité et le nombre de participants"
$page_size = 18;
$offset = $page_size * $page;


$equipmentsCondition = [];
if ($equipementA != "indifférent") {
    array_push($equipmentsCondition, "S1.equipementaudio ='$equipementA'");
}

if ($equipementV != "indifférent") {
    array_push($equipmentsCondition, "S1.equipementvisuel ='$equipementV'");
}
$equipmentsCondition = implode(" AND ", $equipmentsCondition);


$orderConditions = ["NOT (S1.capacite >= '$nbParticipant')", "ABS(S1.capacite - $nbParticipant)"];
if ($equipmentsCondition != "") {
    $orderConditions = ["NOT ($equipmentsCondition)"] + $orderConditions;
}
$orderConditions = implode(", ", $orderConditions);

$selectEquipement = $equipmentsCondition;
if ($selectEquipement == "") {
    $selectEquipement = "true";
}

$req =
    "SELECT *, (S1.capacite >= '$nbParticipant') as canFit, $selectEquipement as matchEquipement
            FROM salle S1 
            WHERE 
                 S1.num_salle NOT IN (
                    SELECT S2.num_salle 
                    FROM salle S2 , reunion R2 
                    WHERE R2.num_salle = S2.num_salle 
                        AND R2.reuniondate = '$DateR' 
                        AND '$dtimedebut' < ADDTIME( R2.heuredebut , R2.reunionduree ) 
                        AND ADDTIME('$dtimedebut' , '$dtime2') >  R2.heuredebut
                )
                AND NOT EXISTS (
                    SELECT *
                    FROM tempsReservation T
                    WHERE T.num_salle = S1.num_salle
                    AND T.until > CURRENT_TIMESTAMP 
                )

            ORDER BY $orderConditions
            ";


$recherche = mysqli_query($conn, $req);
if ($recherche == false) {
    echo "Error: " . mysqli_error($conn);
    exit();
}


$num_rows = mysqli_num_rows($recherche);

$statment = $conn->prepare("SELECT COUNT(1) as nombre_salles
                FROM salle s , reunion r 
                WHERE r.num_salle = s.num_salle
                AND r.reuniondate = ?
                AND ? > ADDTIME( r.heuredebut , r.reunionduree )
                AND ADDTIME(? , ?) <  r.heuredebut");
$statment->bind_param("ssss", $DateR, $dtimedebut, $dtimedebut, $dtime2);
$statment->execute();
$nombre_salles = $statment->get_result()->fetch_assoc()["nombre_salles"];
$statment->close();


if ($num_rows == 0) {
    echo "
        <html>
            <head>
                <meta charset='utf-8'/>
                <title></title>
                <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            </head>
            <body>
                <script>
                    swal('Désolé.. aucune salle n\'est disponible sur ce créneau !', 'Vous allez être redirigé..', 'error')
                        .then(value => {
                            window.location = 'accueil.php';
                        })
                </script>
            </body>
        </html>";
}

$recherche->close();

//EXECUTION DE LA REQUETE
$exploded = explode("\n", $req);
$exploded[0] = "SELECT COUNT(1)";
$count_req = implode("\n", $exploded);
$recherche_count = mysqli_query($conn, $count_req);
if ($recherche_count == false) {
    echo "Error: " . mysqli_error($conn) . "<br>";
    echo "Req: " . $count_req;
    exit();
}
$nbLignes = $recherche_count->fetch_row()[0];
$recherche_count->close();
$page_count = ceil($nbLignes / $page_size);
$recherche = mysqli_query($conn, "$req LIMIT $page_size OFFSET $offset");

/// Création du tableau d'affichage
?>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Reservation</title>

    <link rel="stylesheet" href="/css/pageReservationSalle.css">

    <?php include_once "common/libraries.php" ?>
</head>
<body>

<div>
    <img class="full-image" src="/res/photo/1.jpg" alt="">
</div>
<div class="reservation-area">
    <div class="table">
        <h2>Reservez la salle désirée</h2>
        <table class="style-table">
            <thead>
            <tr>
                <th>Numero de la salle</th>
                <th>Capacité</th>
                <th>Libellé</th>
                <th>Equipement Visuel</th>
                <th>Equipement Audio</th>
                <!--                <th>Match capacité</th>-->
                <!--                <th>Match equipement</th>-->
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $matchEquipment = true;
            $matchCapacity = true;
            while ($data = $recherche->fetch_row()) {
                if ($data[5] == 0 && $matchCapacity) {
                    $matchCapacity = false;
                    $matchEquipment = false;
                    echo "<tr><td colspan='6' class='no-match'>Les salles suivantes ne respectent pas les contraintes de capacité et / ou d'équipements</td></tr>";
                }

                if ($data[6] == 0 && $matchEquipment) {
                    $matchEquipment = false;
                    echo "<tr><td colspan='6' class='no-match'>$data[6] Les salles suivantes ne respectent pas les contraintes d'équipements</td></tr>";
                }


                echo "<tr>";

                for ($k = 0; $k <= 4; $k++) {
                    echo "<td>$data[$k]</td>";
                }
                echo "<td><a href='ajout-participant.php?DateR=$DateR&NomEv=$NomEv&num_salle=$data[0]&DureeR=$DureeR&Hdebut=$dtimedebut&nbParticipant=$nbParticipant&Description=$Description&recurrence=$recurrence&freqreccu=$freqreccu' class='button-style-1'>Reserver</a></td>";

                echo "</tr>";

            }
            ?>
            <! Fin du corps du tableau !>
            </tbody>
            <! Fin du tableau !>
        </table>
    </div>
    <div class="button-tab-area">
        <?php
        //Gere la pagination des salles

        $reference_page = $page + 1;
        $next_page = $reference_page + 1;
        $return_page = $reference_page - 1;

        echo "page $reference_page/$page_count ";
        echo "<div>";
        if ($return_page > 0) {
            echo "
                    <a href='reservation-salle.php?page=$return_page'>
                        <button class='clickable button-style-1'>Retour</button>
                    </a>
                    ";
        }

        if ($next_page <= $page_count) {
            echo "
                    <a href='reservation-salle.php?page=$next_page'>
                        <button class='clickable button-style-1'>Suivant</button>
                    </a>
                    ";
        }
        echo "</div>";
        ?>
    </div>
</div>

<?php
/// Fermeture de la connexion
$conn->close();
?>
</body>
</html>
