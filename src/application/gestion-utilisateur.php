<?php
session_start();

require_once "debug.php";

require "authentication/check-login.php";
require 'authentication/check-admin.php';

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';


if (isset($_POST['modifier'])) {
    //Récupération des paramètres du formulaire
    $idUtilisateur = $_POST['idUtilisateur'];
    $email = $_POST['email'];
    $statut = $_POST['statut'];
    $password = $_POST['password'];


//Enregistrement dans la base de données
//Ecriture de la requête
    $requete = "UPDATE utilisateur SET email='$email', statut='$statut', password='$password'  WHERE id_utilisateur='$idUtilisateur'";
    $resultat = mysqli_query($conn, $requete);


}

if (isset($_POST['envoi'])) {
    //Récupération des paramètres du formulaire
    $email = $_POST['email'];
    $statut = $_POST['statut'];
    $password = $_POST['password'];

    //On teste si l'utilisateurn'existe pas déjà dans la base de données
    $testExistance = $conn->query("SELECT * FROM utilisateur WHERE email='$email'");

    if (mysqli_num_rows($testExistance)) {
        //rien
    } else {
        $requete = "INSERT INTO utilisateur (id_utilisateur,email,statut,password) VALUES (NULL,'$email','$statut',' $password')";
        $res = mysqli_query($conn, $requete);

    }
}

/**
* Gere la barre de recherche et la pagination
*/
$search = "";
$page = 0;
if (isset($_GET['search'])) {
    $search = htmlspecialchars($_GET['search']);
}

if (isset($_GET['page'])) {
    $page = $_GET['page'] - 1;
}

if ($page < 0) {
    $page = 0;
}

$page_size = 20;
$start = $page * $page_size;

$req_count = "SELECT count(1) FROM utilisateur u WHERE u.email LIKE '%$search%'";
$recherche_count = mysqli_query($conn, $req_count);
$count = ceil($recherche_count->fetch_row()[0] / $page_size);
$recherche_count->close();

$req = "SELECT * FROM utilisateur u WHERE u.email LIKE '%$search%' LIMIT $page_size OFFSET $start";
$recherche = mysqli_query($conn, $req);
/// Création du tableau d'affichage
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <META http-equiv="Cache-Control" content="no-cache">
    <META http-equiv="Pragma" content="no-cache">
    <META http-equiv="Expires" content="0">
    <title>Liste Utilisateurs</title>
    <link rel="stylesheet" href="/css/gestionUtilisateurs.css">
    <script rel="script" src="/js/script.js"></script>
    <script rel="script" src="/js/validation.js"></script>

    <?php include_once "common/libraries.php" ?>
</head>

<body>

<?php include_once "common/navigation.php" ?>

<div class="content">
    <div class="search-area">
        <form method="GET">
            <input type="text" placeholder="Entrer une adresse mail" name="search" class="search-value" id="search"
                   value="<?= $search ?>">
            <input type="hidden" value="1" name="page">
            <input class="button-style-2 clickable" type="submit" value="rechercher">
        </form>
        <button class="add-button button-style-2" onclick="reveal('ajouter')">
            <img src="/res/icon/add.svg">
        </button>
    </div>
    <hr>

    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Adresse mail</th>
            <th>Statut</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tableBody">
        <?php

        /// Tant quil reste des lignes à afficher...
        while ($data = $recherche->fetch_row()) {
            /// ...on ajoute une ligne au tableau,...
            ?>
            <tr>
                <?php
                /// ...pour chaque colonne (de la ligne)...
                for ($k = 0; $k <= 2; $k++) {
                    /// ...On affiche l'information correspondante
                    echo "<td>$data[$k]</td>";
                }

                $data_object = json_encode(array(
                    "idUtilisateur" => $data[0],
                    "email" => $data[1],
                    "statut" => $data[2],
                    "password" => $data[3]
                ));
                    /// Bouton qui permet de modifier un contact
                /// Bouton qui permet de supprimer un contact
                echo "
		 		    <td>
                        <img src='/res/icon/edit.svg' onclick='openPopupWith($data_object); reveal(\"editer\")' class='link-icon'> 
                        <a href='/application/requests/reqsuppressionUtilisateur.php?idUtilisateur=$data[0]' class='link-icon' onclick='return confirm(\"Etes-vous sur de votre choix ?\");'>
                            <img src='/res/icon/delete.svg'>
                        </a>
                    </td>
		 		";
                /// Fin de la ligne

                ?>
            </tr>
            <?php
        }
        ?>
        <! Fin du corps du tableau !>
        </tbody>
        <! Fin du tableau !>
    </table>
      <!-- Pagination des pages -->
    <div class="area-page">
        <?php
        $reference_page = $page + 1;
        $next_page = $reference_page + 1;
        $return_page = $reference_page - 1;

        echo "page $reference_page/$count";
        echo "<div>";
        if ($return_page > 0) {
            echo "
                    <a href='gestion-utilisateur.php?page=$return_page&search=$search'>
                        <button class='clickable button-style-2'>Retour</button>
                    </a>
                    ";
        }

        if ($next_page <= $count) {
            echo "
                    <a href='gestion-utilisateur.php?page=$next_page&search=$search'>
                        <button class='clickable button-style-2'>Suivant</button>
                    </a>
                    ";
        }
        echo "</div>";
        ?>
    </div>
</div>
<!-- Formulaire de modification d'utilisateur en forme de pop up-->
<div id="dim" onclick="hide()"></div>
<div id="popup">
    <div id="modification-popup" class="content-popup">
        <form action="gestion-utilisateur.php" method="POST">
            <div>
                Identifiant de l'utilisateur : <input type="text" name="idUtilisateur" id="idUtilisateur" readonly>
            </div>
            <div>
                Adresse Mail : <input type="email" name="email" id="email" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,3}"
                                      placeholder="Ex: prenom.nom@ut-capitole.fr" required>
            </div>
            <div>
                Statut : <select name="statut" id="statut">
                    <option value="utilisateur">utilisateur</option>
                    <option value="superutilisateur">superutilisateur</option>
                </select>
            </div>
            <div>
                Mot de passe : <input type="text" name="password" id="password" pattern="[a-zA-Z0-9]{6}"
                                      placeholder="6 caracteres max" required>
            </div>
            <div class="button-area">
                <input type="reset" name="reset">
                <input type="submit" name="modifier" onclick="" value="Modifier">
            </div>
        </form>
        <!-- gestion des erreurs du formulaire de modification d'utilisateurs-->
        <?php
        if (isset($_POST['modifier'])) {
            if (!$resultat) {
                echo "<script>swal('Ouups !', 'L\'Utilisateur n\'a pas pu etre mis a jour !', 'error');</script>";
            } else {
                echo "
                    <script>swal('Parfait !', 'L\'utilisateur a été mis a jour !', 'success');</script>
                    <script>
                        if ( window.history.replaceState ) {
                            window.history.replaceState( null, null, window.location.href );
                        }
                    </script>
                ";
            }
        }
        ?>
    </div>
    <!-- Formulaire d'ajout de salles en forme de pop up-->
    <div id="add-popup" class="content-popup">
        <form class="col-offset-lg-2 col-lg-3" action="gestion-utilisateur.php" method="post">
            <legend>Nouvel utilisateur</legend>
            <div>
                <label for="text">Adresse Mail : </label>
                <input type="email" name="email" class="form-control" pattern="[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,3}"
                       placeholder="Ex: prenom.nom@ut-capitole.fr" required>
            </div>
            <div>
                <label for="text">Status : </label>
                <select name="statut" class="form-control">
                    <option value="utilisateur">utilisateur</option>
                    <option value="superutilisateur">superutilisateur</option>
                </select>
            </div>
            <div>
                <label for="text">Mot de passe : </label>
                <input type="password" name="password" id="pass" class="form-control" placeholder="6 caracteres max"
                       pattern="[a-zA-Z0-9]{6}" required>
                <button type="button" id="eye" onclick="test();">Affi/Masquer</button>


            </div>
            <div class="button-area">
                <input type="button" name="generer" value="Générer password" onclick="mdpauto(pass);"/>
                <input type="reset" name="reset">
                <input type="submit" name="envoi" value="Ajouter">
            </div>
        </form>
        <!-- gestion des erreurs du formulaire d'ajout des utilisateurs-->
        <?php
        if (isset($_POST['envoi'])) {
            if (mysqli_num_rows($testExistance)) {
                echo "
                    <script>swal('Ouups !', 'L\'Utilisateur n\'a pas pu etre ajouté !', 'error');</script>
                    <script>
                        if ( window.history.replaceState ) {
                            window.history.replaceState( null, null, window.location.href );
                        }
                    </script>
                ";
            } elseif ($res) {
                echo "
                    <script>swal('Parfait !', 'L\'utilisateur a été ajouté !', 'success');</script>
                    <script>
                        if ( window.history.replaceState ) {
                            window.history.replaceState( null, null, window.location.href );
                        }
                    </script>
                ";
            } else {
                echo "
                    <script>swal('Ouups !', 'L\'Utilisateur n\'a pas pu etre ajouté !', 'error');</script>
                    <script>
                        if ( window.history.replaceState ) {
                            window.history.replaceState( null, null, window.location.href );
                        }
                    </script>
                ";
            }
        }
        ?>
    </div>
</div>

</body>
</html>
