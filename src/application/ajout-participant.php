<?php
session_start();

require_once "debug.php";

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';



$emailSession = $_SESSION['email'];


/**
 * Redirige vers l'url mis en paramètre
 * @param string $url
 */
function redirectTo(string $url) {
//    $redirect_url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    header("Location: {$url}", true, 303);
    exit();
}

/**
 * Récupère l'id de la table utilisateur correspondant à l'utilisateur
 * @param string $sessionEmail
 * @param mysqli $conn
 * @return mixed
 */
function getUserID(string $sessionEmail, mysqli $conn) {
    $statement = $conn->prepare("SELECT id_utilisateur FROM utilisateur WHERE email = ? LIMIT 1");
    $statement->bind_param("s", $sessionEmail);
    $statement->execute();
    $result = $statement->get_result();
    $data = $result->fetch_assoc();
    $statement->close();

    if ($data == null) {
        $redirect_url = "/index.php";
        header("Location: {$redirect_url}", true, 303);
        exit();
    }

    return $data['id_utilisateur'];
}

/**
 *
 * @param mysqli $conn
 * @param int $userID
 * @param string $num_salle
 * @return bool
 */
function tempMeetingCanBeBoocked(mysqli $conn, int $userID, string $num_salle): bool {
    try {
        $statement = $conn->prepare("
            SELECT id_utilisateur FROM tempsReservation
            WHERE num_salle = ?
                AND until > CURRENT_TIMESTAMP
        ");

        $statement->bind_param("s", $num_salle);
        $statement->execute();
        $result = $statement->get_result();
        if ($result->num_rows == 0) {
            return true;
        }

        $bookedUser = $result->fetch_assoc()["id_utilisateur"];

        return $bookedUser == $userID;
    } finally {
        $statement->close();
    }
}

/**
 * Vérifie si le numéro de la salle est bien
 * récupérable et bloque la salle en question durant 15 minutes.
 * @param mysqli $conn
 * @param int $userID
 */
function reserveTempMeeting(mysqli $conn, int $userID) {
    function bookingFailure() {
        echo "Impossible de réserver la salle, redirection dans 5 secondes..";
        echo "<script>setTimeout(() => {
            window.location = 'reservation-salle.php';
        }, 5000)</script>";

        exit();
    }


    if (!isset($_GET['num_salle'])) {
        redirectTo("reservation-salle.php");
    }

    $num_salle = $_GET['num_salle'];

    if (!tempMeetingCanBeBoocked($conn, $userID, $num_salle)) {
        bookingFailure();
    }

    $statement = $conn->prepare(
        "INSERT INTO tempsReservation (num_salle, id_utilisateur, until) VALUES (?,?, now() + INTERVAL 15 MINUTE)
        ON DUPLICATE KEY UPDATE
            id_utilisateur = ?,
            until = DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 15 MINUTE)"
    );

    $statement->bind_param("sii", $num_salle, $userID, $userID);
    $success = $statement->execute();
    $statement->close();

    if (!$success) {
        bookingFailure();
    }
}

/**
 * Retourne un tableau reunion avec l'od de la reunion et son nom pour l'utilisateur courant
 * @param mysqli $conn
 * @param int $userID
 * @return array
 */
function fetchMeetingsForCurrentUser(mysqli $conn, int $userID): array {
    $statement = $conn->prepare("
        SELECT id_reunion, reunionnom 
        FROM reunion
        WHERE id_utilisateur = ?
        ORDER BY id_reunion DESC
    ");

    $statement->bind_param("i", $userID);
    $statement->execute();
    $result = $statement->get_result();

    $reunions = [];
    while ($data = $result->fetch_assoc()) {
        array_push($reunions, [
            "id" => $data["id_reunion"],
            "nom" => $data["reunionnom"]
        ]);
    }

    $statement->close();

    return $reunions;
}

/**
 * Récupère les valeurs de l'url ainsi que l'id de l'event google calendar
 * pour ajouter un enregistrement dans la table reunion de la bdd ainsi que des
 * enregistrements dans la table participant.
 * @param mysqli $conn
 * @param int $userID
 * @param string $emailSession
 */
function validateMeeting(mysqli $conn, int $userID, string $emailSession) {
    $DateR = $_GET['DateR'];
    $NomEv = $_GET['NomEv'];
    $DureeR = $_GET['DureeR'];
    $Hdebut = $_GET['Hdebut'];
    $nbParticipant = $_GET['nbParticipant'];
    $Description = $_GET['Description'];
    $num_salle = $_GET['num_salle'];
    $recurrence = $_GET['recurrence'];
    $freqreccu = $_GET['freqreccu'];

    try {
        $conn-> begin_transaction();

        $statement = $conn->prepare("INSERT INTO reunion (reuniondate, reunionnom, heuredebut, reunionduree, recurrence, freqreccu, description, nbparticipant, num_salle, id_utilisateur) VALUES (?,?,?,?,?,?,?,?,?,?)");
        $statement->bind_param("sssssisssi", $DateR, $NomEv, $Hdebut, $DureeR, $recurrence, $freqreccu, $Description, $nbParticipant, $num_salle, $userID);
        $result = $statement->execute();
        $id_reunion = $statement->insert_id;

        if ($result == false) {
            echo "Impossible de créer la réunion...<br>";
            $error = $statement->error;
            echo "Error: $error";
            echo "<script>setTimeout(() => {
                window.location = 'reservation-salle.php';
            }, 10000)</script>";

            throw new Exception();
        }

        $participants = $_POST['participants'];
        foreach ($participants as $email) {
            try {
                $participantStatement = $conn->prepare("INSERT IGNORE INTO participant (emailparticipant, id_reunion) VALUES (?,?)");
                $participantStatement->bind_param("si", $email, $id_reunion);
                $participantStatement->execute();
            } finally {
                $participantStatement->close();
            }
        }

        $_SESSION['success'] = true;

        $insertedEvent = insertGoogleEvent($NomEv, $Description, $Hdebut, $DureeR, $num_salle, $recurrence,$freqreccu, $participants, $emailSession);

        $statement = $conn->prepare("
            UPDATE reunion
            SET eventId = ?
            WHERE id_reunion = ?
        ");

        $statement->bind_param("si", $insertedEvent->id, $id_reunion);
        if ($statement->execute() == false) {
            throw new Exception();
        }


        $conn->commit();
        echo "
        <html>
            <head>
                <meta charset='utf-8'/>
                <title>Confirmation</title>
                <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
            </head>
            <body>
                <script>
                swal({
                    title: 'Votre Réservation a bien été prise en compte !',
                    text: 'Une invitation a été envoyé a tous les participants',
                    type: 'success'
                }).then(function() {
                    window.location = '/application/accueil.php';
                });</script>
            </body>
        </html>";
    } catch (Exception $e) {
        $conn->rollback();

        echo $e;
        exit();
    } finally {
        if (!is_bool($statement)) {
            $statement->close();
        }
    }
}

/**
 * Génère un tableau d'un tableau d'adresses mail des participants
 * @param $attendees array
 * @return array
 */
function generateAttendeesArray($attendees): array {
    function generateAttendee($attendee): array {
        return ["email" => $attendee];
    }

    return array_map('generateAttendee', $attendees);
}

/**
 * insert les éléments de l'événement à google calendar et retourne ce dernier
 * @param $name string
 * @param $description string
 * @param $start string
 * @param $duration string
 * @param $location string
 * @param $recurrence string
 * @param $freqreccu int
 * @param $attendees array
 * @param $emailSession string
 * @return Google_Service_Calendar_Event
 * @throws Exception
 */
function insertGoogleEvent($name, $description, $start, $duration, $location, $recurrence, $freqreccu, $attendees, $emailSession): Google_Service_Calendar_Event {
    $manager = GoogleAPIManager::getInstance();
    $formattedStart = new DateTime($start);
    $formattedStart = $formattedStart->format(DateTime::ATOM);
    [$hours, $minutes, $seconds] = explode(":", $duration);
    $formattedEnd = new DateTime($start);
    $formattedEnd = $formattedEnd->add(new DateInterval("PT${hours}H${minutes}M${seconds}S"));
    $formattedEnd = $formattedEnd->format(DateTime::ATOM);
    $attendeesEmails = $manager->generateAttendeesArray(array_merge($attendees, [$emailSession]));

    return $manager->createEvent(new Google_Service_Calendar_Event(array(
        'summary' => $name,
        'location' => $location,
        'description' => $description,
        'start' => array(
            'dateTime' => $formattedStart,
            'timeZone' => 'Europe/Paris',
        ),
        'end' => array(
            'dateTime' => $formattedEnd,
            'timeZone' => 'Europe/Paris',
        ),
        'recurrence' => array(
            'RRULE:FREQ='.$recurrence.';COUNT='.$freqreccu.''
        ),
        'attendees' => $attendeesEmails,
        'reminders' => array(
            'useDefault' => FALSE,
            'overrides' => array(
                array('method' => 'email', 'minutes' => 24 * 60),
                array('method' => 'popup', 'minutes' => 10),
            ),
        ),
    )));
}


$userID = getUserID($emailSession, $conn);

$success = false;
if (isset($_SESSION['result'])) {
    $success = $_SESSION['result'];
    unset($_SESSION['result']);
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    reserveTempMeeting($conn, $userID);
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    validateMeeting($conn, $userID, $emailSession);
}


$reunions = fetchMeetingsForCurrentUser($conn, $userID);
$nbLignes = count($reunions);

?>
<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Ajout Participant</title>
    <link rel="stylesheet" href="/css/pageAjoutParticipant.css">
    <script rel="script" src="/js/validation.js"></script>
    <script rel="script" src="/js/script.js"></script>
    <script rel="script" src="/js/add_participant.js"></script>

    <?php include_once "common/libraries.php" ?>
?>
</head>
<body>
<div>
    <img class="full-image" src="/res/photo/1.jpg" alt="">
</div>

<form method="POST">
    <div>
        <h2>Ajout des participants à la réunion</h2>
        <i class="small-text">
            L'adresse mail saisie doit finir par un point-virgule pour être validée ex :"lorem@gmail.com;".
            <br>
            <br>
            Pour ajouter une liste d'e-mails par un copier-coller, veillez à ce que chaque mail finisse par un point-virgule ex :"lorem@gmail.com;  ipsum@gmail.com; dolor@gmail.com;".
        </i>
    </div>
    <div class="c-input">
        <div>
            <!---<label for="filtre">Réunion</label>
            <select class="input-style" name="filtre">
                <?php
            for ($i=0; $i < min(4, $nbLignes); $i++) {
                ["id" => $id, "nom" => $nom] = $reunions[$i];
                echo "<option value='$nom' > $id $nom</option>";
            }
            ?>
            </select>--->
        </div>
        <div class="style-animation">
            <input type="text" id="email" class="input-style-animated">
            <label for="email">Adresse mail</label>
        </div>
    </div>

    <div>
        <h2>Participants</h2>
        <div id="liste-participant"></div>
    </div>
    <input type="submit" class="button-style-1 clickable" value="Valider la réservation" name="envoi"/>
    <div class="div-arrow">
        <a href='<?php echo $_SERVER["HTTP_REFERER"]; ?>'>
            <i class="fas fa-arrow-left"></i>
        </a>
    </div>
</form>
<script>
    document.getElementById('email').oninput = (event) => {
        console.log(event.target.value);
        ajouterSiNecessaire(event,"liste-participant");
    }
</script>
<?php
if(isset($_SESSION['result'])) {
    if($success) {
        echo "<script>Swal.fire(
            'Echec..!!',
            'Veuillez réessayer !',
            'error'
          );</script>";
        echo "<script>if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }</script>";
    } else {
        echo "<script>const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })
          
          swalWithBootstrapButtons.fire({
            title: 'Parfait.. ! La réunion a bien été créée',
            text: 'Vous allez être redirigé',
            icon: 'success',
            showCancelButton: false,
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
             window.location = 'reservation-salle.php';
            } else if (
              /* Read more about handling dismissals below */
              result.dismiss === Swal.DismissReason.cancel
            ) {
              swalWithBootstrapButtons.fire(
                'Validation en cours...',
                'Redirection vers la page Accueil',
                'success'
              )
              setTimeout(\"window.location='accueilAdmin.php'\",5000);
            } 
          })</script>";

        echo "<script>if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }</script>";
    }

}
?>
</body>
</html>
