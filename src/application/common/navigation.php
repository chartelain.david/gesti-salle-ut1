<?php

function getPageOfLink(string $link) {
    $base = explode("/", $link);
    $length = count($base);
    return $base[$length - 1];
}

function currentPage(): string {
    return getPageOfLink($_SERVER['REQUEST_URI']);
}

function createLink(string $url, string $name) {
    $classes = getPageOfLink($url) === currentPage() ? "indication" : "";
    echo "<li><a href='$url' class='$classes'>$name</a></li>";
}
?>

<nav>
    <input type="checkbox" id="check">
    <label for="check" class="checkbtn clickable">
        <i class="fas fa-bars"></i>
    </label>
    <label class="logo">GestiSalleUT1</label>
    <ul>
        <li style= "color: white;"><?php echo $_SESSION['email']; ?></li>
        <?php
        createLink("/application/accueil.php", "Réservation");
        createLink("/application/liste-reunion.php", "Mes réunions" );
        ?>

<!--        ajout des liens si l'utilisateur est un superutilisateur-->
        <?php
        if ($_SESSION['status'] === "superutilisateur") {
            createLink("/application/gestion-salles.php", "Gestion des salles");
            createLink("/application/gestion-utilisateur.php","Gestion utilisateurs" );
        }
        ?>

        <li><a href="/application/authentication/logout.php">Déconnexion</a></li>
    </ul>
</nav>