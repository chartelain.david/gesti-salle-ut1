<?php
session_start();

require_once "debug.php";

require "authentication/check-login.php";
require 'authentication/check-admin.php';
require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';

if (isset($_POST['modif'])) {
    //Récupération des paramètres du formulaire
    $numsalle = $_POST['numsalle'];
    $capacite = $_POST['capacite'];
    $libelle = $_POST['libelle'];
    $equipementV = $_POST['equipementvisuel'];
    $equipementA = $_POST['equipementaudio'];

//Enregistrement dans la base de données
//Ecriture de la requête
    $requete = "UPDATE salle SET num_salle='$numsalle', capacite='$capacite', libelle='$libelle' , equipementvisuel='$equipementV' , equipementaudio='$equipementA' WHERE num_salle='$numsalle'";
    $result = mysqli_query($conn, $requete);
}

if (isset($_POST['envoi'])) {

//Récupération des paramètres du formulaire 
    $numsalle = $_POST['numsalle'];
    $capacite = $_POST['capacite'];
    $libelle = $_POST['libelle'];
    $equipementV = $_POST['equipementvisuel'];
    $equipementA = $_POST['equipementaudio'];

    //On teste si le numero de salle n'existe pas déjà dans la base de données
    $testExistance = $conn->query("SELECT * FROM salle WHERE num_salle='$numsalle'");
    // Si il existe déjà,
    if (mysqli_num_rows($testExistance)) {
        //echo ("ATTENTION : Salle non ajouté, déjà existant dans la base de données");
    } else {
        // Préparatin de la requete
        $requete = "INSERT INTO salle (num_salle,capacite,libelle,equipementvisuel,equipementaudio) VALUES ('$numsalle','$capacite','$libelle', '$equipementV','$equipementA')";
        $resu = mysqli_query($conn, $requete);
    }
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Liste Salles</title>

    <link rel="stylesheet" href="/css/gestionUtilisateurs.css">
    <script rel="script" src="/js/script.js"></script>
    <script rel="script" src="/js/validation.js"></script>

    <?php include_once "common/libraries.php" ?>
</head>
<body>

<?php include_once "common/navigation.php" ?>

<?php
/**
* Gere la barre de recherche et la pagination
*/
$search = "";
$page = 0;
if (isset($_GET['search'])) {
    $search = htmlspecialchars($_GET['search']);
}

if (isset($_GET['page'])) {
    $page = $_GET['page'] - 1;
}

if ($page < 0) {
    $page = 0;
}

$page_size = 20;
$start = $page * $page_size;

$req_count = "SELECT count(1) FROM salle s WHERE s.num_salle LIKE '%$search%'";
$recherche_count = mysqli_query($conn, $req_count);
$count = ceil($recherche_count->fetch_row()[0] / $page_size);
$recherche_count->close();

$req = "SELECT * FROM salle s WHERE s.num_salle LIKE '%$search%' LIMIT $page_size OFFSET $start";
$recherche = mysqli_query($conn, $req);
?>
<div class="content">
    <div class="search-area">
        <form method="GET">
            <input type="text" placeholder="Entrer un numéro de salle" name="search" class="search-value" id="search"
                   value="<?php echo $search ?>">
            <input type="hidden" value="1" name="page">
            <input class="button-style-2 clickable" type="submit" value="rechercher">
        </form>
        <button class="add-button button-style-2" onclick="reveal('ajouter')">
            <img src="/res/icon/add.svg">
        </button>
    </div>
    <hr>

    <table>
        <thead>
        <tr>
            <th> Numero de salle</th>
            <th> Capacité</th>
            <th> Libélle</th>
            <th>Equipement Visuel</th>
            <th>Equipement Audio</th>
        </tr>
        </thead>
        <tbody id="tableBody">
        <?php

        /// Tant quil reste des lignes à afficher...
        while ($data = $recherche->fetch_row()) {
            /// ...on ajoute une ligne au tableau,...
            ?>
            <tr>
                <?php
                /// ...pour chaque colonne (de la ligne)...
                for ($k = 0; $k <= 4; $k++) {
                    /// ...On affiche l'information correspondante
                    echo "<td>$data[$k]</td>";
                }

                $data_object = json_encode(array(
                    "numsalle" => $data[0],
                    "capacite" => $data[1],
                    "libelle" => $data[2],
                    "equipementvisuel" => $data[3],
                    "equipementaudio" => $data[4]
                ));
                /// Bouton qui permet de modifier un contact
                /// Bouton qui permet de supprimer un contact
                echo "
		 		<td>
                        <img src='/res/icon/edit.svg' onclick='openPopupWith2($data_object); reveal(\"editer\")' class='link-icon'> 
                        <a href='/application/requests/reqsuppressionSalle.php?numsalle=$data[0]' class='link-icon' onclick='return confirm(\"Etes-vous sur de votre choix ?\");'>
                            <img src='/res/icon/delete.svg'>
                        </a>
                       </td>
		 		";
                /// Fin de la ligne

                ?>
            </tr>
            <?php
        }
        ?>
        <! Fin du corps du tableau !>
        </tbody>
        <! Fin du tableau !>
    </table>
    <!-- Pagination des pages -->
    <div class="area-page">
        <?php
        $reference_page = $page + 1;
        $next_page = $reference_page + 1;
        $return_page = $reference_page - 1;

        echo "page $reference_page/$count";
        echo "<div>";
        if ($return_page > 0) {
            echo "
                    <a href='gestion-salles.php?page=$return_page&search=$search'>
                        <button class='clickable button-style-2'>Retour</button>
                    </a>
                    ";
        }

        if ($next_page <= $count) {
            echo "
                    <a href='gestion-salles.php?page=$next_page&search=$search'>
                        <button class='clickable button-style-2'>Suivant</button>
                    </a>
                    ";
        }
        echo "</div>";
        ?>
    </div>
</div>
<!-- Formulaire de modification de salles en forme de pop up-->
<div id="dim" onclick="hide()"></div>
<div id="popup">
    <div id="modification-popup" class="content-popup">
        <form action="gestion-salles.php" method="post">
            <div>
                Numero de la salle : <input type="text" name="numsalle" id="numsalle">
            </div>
            <div>
                Capacité : <input type="number" name="capacite" id="capacite" required>
            </div>
            <div>
                Libellé : <input type="text" name="libelle" id="libelle" required>
            </div>
            <div>
                Equipement visuel : <select name="equipementvisuel" id="equipementvisuel">
                    <option value="oui">oui</option>
                    <option value="non">non</option>
                </select>
            </div>
            <div>
                Equipement audio : <select name="equipementaudio" id="equipementaudio">
                    <option value="oui">oui</option>
                    <option value="non">non</option>
                </select>
            </div>
            <div class="button-area">
                <input type="reset" name="reset">
                <input type="submit" name="modif" value="Modifier">
            </div>
        </form>
        <!-- gestion des erreurs du formulaire de modification-->
        <?php
        if (isset($_POST['modif'])) {
            if (!$result) {
                echo "<script>swal('Ouups...! ', 'La salle n\'a pas pu etre mise a jour !', 'error');</script>";
                echo "<script>if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }</script>";
            } else {
                echo "<script>swal('Excellent !', 'La salle a été mise a jour avec succès !', 'success');</script>";
                echo "<script>if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }</script>";
            }
        }
        ?>

    </div>
    <!-- Formulaire d'ajout de salles en forme de pop up-->
    <div id="add-popup" class="content-popup">
        <form class="col-offset-lg-2 col-lg-3" action="gestion-salles.php" method="post">
            <legend>Nouvelle salle</legend>
            <div>
                Numero de la salle : <input type="text" name="numsalle" id="numsalle">
            </div>
            <div>
                Capacité : <input type="number" name="capacite" id="capacite" required>
            </div>
            <div>
                Libellé : <input type="text" name="libelle" id="libelle" required>
            </div>
            <div>
                Equipement visuel : <select name="equipementvisuel" id="equipementvisuel">
                    <option value="oui">oui</option>
                    <option value="non">non</option>
                </select>
            </div>
            <div>
                Equipement audio : <select name="equipementaudio" id="equipementaudio">
                    <option value="oui">oui</option>
                    <option value="non">non</option>
                </select>
            </div>
            <div class="button-area">
                <input type="reset" name="reset">
                <input type="submit" name="envoi" value="Ajouter">
            </div>
        </form>
        <!-- gestion des erreurs du formulaire d'ajout-->
        <?php
        if (isset($_POST['envoi'])) {
            if (mysqli_num_rows($testExistance)) {
                echo "<script>swal('Ouups...!', 'La salle n\'a pas pu etre ajouté !', 'error');</script>";
                echo "<script>if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }</script>";
            } elseif ($resu) {
                echo "<script>swal('Excellent !', 'La salle a été ajouté !', 'success');</script>";
                echo "<script>if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }</script>";
            } else {
                echo "<script>swal('Ouups...!', La salle n\'a pas pu etre ajouté !', 'error');</script>";
                echo "<script>if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }</script>";
            }
        }
        ?>
    </div>
</div>
</body>
</html>