<?php

if ($_SESSION['status'] !== 'superutilisateur') {
    header("Location: /application/accueil.php");
    exit();
}

?>