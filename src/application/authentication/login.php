<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once $_SERVER['DOCUMENT_ROOT'] . "/connexion.php";

$message = "";
$statut = "";

if (isset($_POST["btnLogin"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];

    $query = $conn->prepare("SELECT * FROM utilisateur WHERE email = ? AND password = ?");
    $query->bind_param("ss", $email, $password);
    $query->execute();

    $result = $query->get_result();
    $num_rows = $result->num_rows;


    if ($num_rows == 1) {
        $row = mysqli_fetch_assoc($result);
        $_SESSION['idSession'] = session_create_id();
        $_SESSION['email'] = $row["email"];
        $_SESSION['status'] = $row["statut"];

        header("Location: /application/accueil.php");
        exit();
    } else {
        include $_SERVER['DOCUMENT_ROOT'] . "/index.php";

        echo "<html>
        <head>
            <meta charset='utf-8'/>
            <title>Confirmation</title>
            <link rel='stylesheet' href='../../css/common.css'>
            <link rel='stylesheet' href='../../css/gestionUtilisateurs.css'>
            <script rel='script' src='../../js/script.js'></script>
            <script rel='script' src='../../js/validation.js'></script>
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
        </head>
        <body>
        <script>swal('Oupps... !', 'Adresse mail ou Mot de passe incorrect !', 'error');</script>
        </body></html>";
    }
} else {
    header("Location: /index.php");
}
?>
