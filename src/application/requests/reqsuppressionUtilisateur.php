<?php
session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';

    
//Récupération des paramètres du formulaire 
$idUtilisateur = $_GET['idUtilisateur']; 

//Requete de supression par rapport à l'id
$requete = "DELETE FROM utilisateur WHERE id_utilisateur= '$idUtilisateur'";
$resultat = mysqli_query($conn, $requete);
  
if(!$resultat) {
  echo "<html>
  <head>
  <meta charset='utf-8'/>
  <title>Confirmation</title>
  <link rel='stylesheet' href='/css/common.css'>
  <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
  <script rel='script' src='/js/script.js'></script>
  <script rel='script' src='/js/validation.js'></script>
  <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
  </head>
  <body>
  <script>swal('Ouups !', 'L\'Utilisateur n\'a pas pu etre supprimé !', 'error');</script>
  </body></html>";
   }
   else {
     echo "<html>
     <head>
         <meta charset='utf-8'/>
         <title>Confirmation</title>
         <link rel='stylesheet' href='/css/common.css'>
         <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
         <script rel='script' src='/js/script.js'></script>
         <script rel='script' src='/js/validation.js'></script>
         <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
     </head>
     <body>
     <script>swal('Parfait !', 'L\'utilisateur a été supprimé !', 'success');</script>
     </body></html>";
   }

$delai=3; // le nombre de secondes
$url='/application/gestion-utilisateur.php'; // ton url
header("Refresh: $delai; $url");
?>
