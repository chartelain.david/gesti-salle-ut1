<?php
session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';

function handleError() {
    echo "
    <html>
        <head>
            <meta charset='utf-8'/>
            <title>Confirmation</title>
            <link rel='stylesheet' href'/css/common.css'>
            <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
            <script rel='script' src='/js/script.js'></script>
            <script rel='script' src='/js/validation.js'></script>
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
        </head>
        <body>
            <script>swal('Ouups ! Désolé..', 'La réunion n\'a pas pu être modifié !', 'error');</script>
        </body>
    </html>";
}

function handleSuccess() {
    echo "
    <html>
        <head>
            <meta charset='utf-8'/>
            <title>Confirmation</title>
            <link rel='stylesheet' href='/css/common.css'>
            <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
            <script rel='script' src='/js/script.js'></script>
            <script rel='script' src='/js/validation.js'></script>
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
        </head>
        <body>
            <script>swal('Parfait !', 'La réunion a été modifié avec succès !', 'success');</script>
        </body>
    </html>";
}

//recupérations des données
$DateR = $_GET['reuniondate'];
$reunionNom = $_GET['reunionnom'];
$DureeR = $_GET['reunionduree'];
$Hdebut = $_GET['heuredebut'];
$nbParticipant = $_GET['nbparticipant'];
$Description = $_GET['description'];
$eventId = $_GET['eventId'];
$numSalle = $_GET['num_salle'];
$id_reunion = $_GET['id_reunion'];

//heure debut/fin formater
$formattedStart = new DateTime($Hdebut);
$formattedStart = $formattedStart->format(DateTime::ATOM);
[$hours, $minutes, $seconds] = explode(":", $DureeR);
$formattedEnd = new DateTime($Hdebut);
$formattedEnd = $formattedEnd->add(new DateInterval("PT${hours}H${minutes}M${seconds}S"));
$formattedEnd = $formattedEnd->format(DateTime::ATOM);

//Enregistrement des modifications dans la bdd
try {
    $conn->begin_transaction();
    $requete = $conn->prepare("UPDATE reunion SET reuniondate=?, reunionnom=?,heuredebut=?, reunionduree=?, description=?, nbparticipant=?, num_salle=? WHERE id_reunion=?");
    $requete->bind_param("sssssisi", $DateR, $reunionNom, $Hdebut, $DureeR, $Description, $nbParticipant, $numSalle, $id_reunion);
    if (!$requete->execute()) {
        throw new Exception();
    }

    //update event google calendar
    $event = GoogleAPIManager::getInstance()->getEvent($eventId);
    if (is_null($event)) {
        throw new Exception();
    }


    $event->setSummary($reunionNom);
    $event->setLocation($numSalle);
    $event->setDescription($Description);
    $event->getStart()->setDateTime($formattedStart);
    $event->getStart()->setTimeZone('Europe/Paris');
    $event->getEnd()->setDateTime($formattedEnd);
    $event->getEnd()->setTimeZone('Europe/Paris');
    $updated = GoogleApiManager::getInstance()->updateEvent($event);

    if (is_null($updated)) {
        throw new Exception();
    }

    $conn->commit();
    handleSuccess();
} catch (Exception $e) {
    $conn->rollback();
    handleError();
} finally {
    $delai = 3; // le nombre de secondes
    $url = '/application/liste-reunion.php'; // ton url
    header("Refresh: $delai; $url");


    if (!empty($requete)) {
        $requete->close();
    }
}

?>