<?php
session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';
    
//Récupération des paramètres du formulaire 
$numreunion = $_GET['id_reunion'];
$request= $conn->prepare('SELECT eventId FROM reunion WHERE id_reunion = ?');
$request->bind_param('i', $numreunion);

if ($request->execute() == false) {
    echo 'Une erreur est survenue';
    exit();
}

$result = $request->get_result();
if ($result == false || $result->num_rows == 0) {
    echo "Impossible de trouver la réunion";
    exit();
}

$eventID = $result->fetch_assoc()["eventId"];
$result->close();

//Suppresion reunion google calendar
$manager = GoogleAPIManager::getInstance();
$manager->deleteEvent($eventID);

//Requete de supression par rapport à l'id
$requete = "DELETE FROM reunion WHERE id_reunion = '$numreunion'";
$resultat = mysqli_query($conn, $requete);
  
if(!$resultat) {
  echo "<html>
  <head>
  <meta charset='utf-8'/>
  <title>Confirmation</title>
  <link rel='stylesheet' href='/css/common.css'>
  <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
  <script rel='script' src='/js/script.js'></script>
  <script rel='script' src='/js/validation.js'></script>
  <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
  </head>
  <body>
  <script>swal('Ouups ! Désolé..', 'La réunion n\'a pas pu etre supprimé !', 'error');</script>
  </body></html>";
   }
   else {
    echo "<html>
    <head>
        <meta charset='utf-8'/>
        <title>Confirmation</title>
        <link rel='stylesheet' href='/css/common.css'>
        <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
        <script rel='script' src='/js/script.js'></script>
        <script rel='script' src='/js/validation.js'></script>
        <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
    </head>
    <body>
    <script>swal('Parfait !', 'La réunion a été supprimé avec succès !', 'success');</script>
    </body></html>";
     }

$delai=3; // le nombre de secondes
$url='/application/liste-reunion.php'; // ton url
header("Refresh: $delai; $url");
?>
