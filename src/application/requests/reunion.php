<?php

session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';

function handleError() {
    echo "
    <html>
        <head>
            <meta charset='utf-8'/>
            <title>Confirmation</title>
            <link rel='stylesheet' href'/css/common.css'>
            <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
            <script rel='script' src='/js/script.js'></script>
            <script rel='script' src='/js/validation.js'></script>
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
        </head>
        <body>
            <script>swal('Ouups ! Désolé..', 'Les participants n\'ont pas pu être modifié !', 'error');</script>
        </body>
    </html>";
}

function handleSuccess() {
    echo "
    <html>
        <head>
            <meta charset='utf-8'/>
            <title>Confirmation</title>
            <link rel='stylesheet' href='/css/common.css'>
            <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
            <script rel='script' src='/js/script.js'></script>
            <script rel='script' src='/js/validation.js'></script>
            <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
        </head>
        <body>
            <script>swal('Parfait !', 'Les particpants ont été modifié avec succès !', 'success');</script>
        </body>
    </html>";
}

function getMeeting()
{
    global $conn;

    $reunion_id = $_GET['reunion_id'];
    $attendees = [];

    try {
        $request = $conn->prepare(
            "SELECT p.emailparticipant as 'attendee'
            FROM participant p
            WHERE p.id_reunion = ?"
        );

        $request->bind_param("i", $reunion_id);

        if ($request->execute()) {
            $result = $request->get_result();
            while ($row = $result->fetch_assoc()) {
                array_push($attendees, $row['attendee']);
            }
        }
    } catch (Exception $e) {
        echo $e;
    } finally {
        if (!empty($request) && $request != false) {
            $request->close();
        }
    }

    echo json_encode($attendees);
}


function editMeetingAttendees()
{
    global $conn;

    $attendees = $_POST["participants"];
    $reunion_id = $_POST["reunion_id"];
    $eventId = $_POST["eventId"];

    if (!$attendees) {
        echo $attendees;
        echo json_encode($_POST);

        exit();
    }

    try {
        $conn->begin_transaction();

        $deleteRequest = $conn->prepare(
            "DELETE from participant WHERE id_reunion = ?"
        );

        $deleteRequest->bind_param("i", $reunion_id);
        if (!$deleteRequest->execute()) {
            $conn->rollback();
            throw new Exception("Unable to remove attendees");
        }

        foreach ($attendees as $attendee) {
            $request = $conn->prepare("INSERT into participant (emailparticipant, id_reunion) VALUES (?,?)");
            $request->bind_param("si", $attendee, $reunion_id);

            if (!$request->execute()) {
                throw new Exception("Unable to insert attendee ('$attendee', '$reunion_id')");
            }
        }

        /*update event google calendar*/
        $manager = GoogleAPIManager::getInstance();
        $event = $manager->getEvent($eventId);

        if (is_null($event)) {
            throw new Exception();
        }

        $event->setAttendees(
            $manager->generateAttendeesArray($attendees)
        );
        $updated = GoogleApiManager::getInstance()->updateEvent($event);

        if (is_null($updated)) {
            throw new Exception();
        }

        $conn->commit();
        handleSuccess();
        header("Location: " . $_SERVER['HTTP_REFERER']);
    } catch (Exception $e) {
        echo $e;
        $conn->rollback();
        handleError();
    } finally {
        if (!empty($deleteRequest) && $deleteRequest != false) {
            $deleteRequest->close();
        }

        if (!empty($request) && $request != false) {
            $request->close();
        }
    }

    echo "Successfully inserted";
}


switch ($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        getMeeting();
        break;

    case 'POST':
        editMeetingAttendees();
        break;

    default:
        throw new BadFunctionCallException("Unsupported method");
}


?>