<?php
session_start();

require $_SERVER['DOCUMENT_ROOT'] . '/connexion.php';
require $_SERVER['DOCUMENT_ROOT'] . '/GoogleAPI/GoogleAPIManager.php';
    
//Récupération des paramètres du formulaire 
$numsalle = $_GET['numsalle']; 

//Requete de supression par rapport à l'id
$requete = "DELETE FROM salle WHERE num_salle= '$numsalle'";
$resultat = mysqli_query($conn, $requete);
  
if(!$resultat) {
  echo "<html>
  <head>
  <meta charset='utf-8'/>
  <title>Confirmation</title>
  <link rel='stylesheet' href='/css/common.css'>
  <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
  <script rel='script' src='/js/script.js'></script>
  <script rel='script' src='/js/validation.js'></script>
  <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
  </head>
  <body>
  <script>swal('Ouups ! Désolé..', 'La salle n\'a pas pu etre supprimé !', 'error');</script>
  </body></html>";
   }
   else {
    echo "<html>
    <head>
        <meta charset='utf-8'/>
        <title>Confirmation</title>
        <link rel='stylesheet' href='/css/common.css'>
        <link rel='stylesheet' href='/css/gestionUtilisateurs.css'>
        <script rel='script' src='/js/script.js'></script>
        <script rel='script' src='/js/validation.js'></script>
        <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
    </head>
    <body>
    <script>swal('Parfait !', 'La salle a été supprimé avec succès !', 'success');</script>
    </body></html>";
     }

$delai=3; // le nombre de secondes
$url='/application/gestion-salles.php'; // ton url
header("Refresh: $delai; $url");
?>
