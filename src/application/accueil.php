<?php
session_start();

require_once "debug.php";

require_once 'authentication/check-login.php';

?>

<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Accueil</title>

    <script rel="script" src="../../js/validation.js"></script>
    <script rel="script" src="../../js/script.js"></script>
    <link rel="stylesheet" href="../../css/acceuilAdmin.css">

    <?php include_once "common/libraries.php" ?>
</head>
<body>


<?php include_once "common/navigation.php" ?>


<div>
    <img class="full-image" src="/res/photo/1.jpg" alt="">
</div>

<div class="popupMeeting">
    <form action="reservation-salle.php" method="GET">
        <div class="display-renseignements">
            <div class="content-input spacing-1">
                <h2 class="grid-title">Renseignements sur la réunion</h2>
                <div class="style-animation">
                    <input type="text" class="input-style-animated" name="NomEv" required>
                    <label for="text"> Nom de la réunion</label>
                </div>
                <div class="two-column">
                    <div>
                        <label for="date">Date de la réunion</label>
                        <input type="date" name="DateR" class="input-style" value="<?php echo date('Y-m-d'); ?>" required>
                    </div>
                    <div class="all-day no-select">
                        <input type="checkbox" class="style-checkbox-1" id="day" name="day">
                        <label for="day" class="clickable">Journée entière</label>
                    </div>
                </div>
                <div class="two-column">
                    <div id="hour">
                        <label for="Hdebut">Heure de début :</label>
                        <input type="time" class="input-style" id="hdebut" name="Hdebut" value="<?php echo date('H:i'); ?>" required>
                    </div>
                    <div id="duration">
                        <label for="text2">Durée :</label>
                        <select class="input-style" id="duree" name="DureeR" required>
                            <option value="00:30:00">30min</option>
                            <option value="00:45:00">45min</option>
                            <option value="01:00:00">1h</option>
                            <option value="01:30:00">1h30min</option>
                            <option value="02:00:00">2h</option>
                            <option value="02:30:00">2h30min</option>
                            <option value="03:00:00">3h</option>
                            <option value="03:30:00">3h30min</option>
                            <option value="04:00:00">4h</option>
                            <option value="04:30:00">4h30min</option>
                            <option value="05:00:00">5h</option>
                            <option value="05:30:00">5h30min</option>
                            <option value="06:00:00">6h</option>
                            <option value="06:30:00">6h30min</option>
                            <option value="07:00:00">7h</option>
                            <option value="07:30:00">7h30min</option>
                            <option value="08:00:00">8h</option>
                            <option value="08:30:00">8h30min</option>
                            <option value="09:00:00">9h</option>
                            <option value="09:30:00">9h30min</option>
                            <option value="10:00:00">10h</option>
                            <option value="10:30:00">10h30min</option>
                            <option value="11:00:00">11h</option>
                            <option value="11:30:00">11h30min</option>
                            <option value="12:00:00">12h</option>
                            <option value="12:30:00">12h30min</option>
                            <option value="13:00:00">13h</option>
                            <option value="13:30:00">13h30min</option>
                            <option value="14:00:00">14h</option>
                            <option value="14:30:00">14h30min</option>
                            <option value="15:00:00">15h</option>
                            <option value="15:30:00">15h30min</option>
                            <option value="16:00:00">16h</option>
                        </select>

                    </div>
                </div>
                <div>
                    <label>Récurrence de la réunion</label>
                    <select class="input-style" name="recurrence">
                        <option value="DAILY">Une seule fois</option>
                        <option value="WEEKLY">Le meme jour de la semaine</option>
                    </select>
                </div>
                <div>
                    <label>Frequence de la recurrence</label>
                    <input type="number" name="freqreccu" placeholder="Saisir un nombre de 1 (pour une réunion) a 365"
                           min="1" max="365" class="input-style" value="1">
                </div>
                <div>
                    <label for="Description">Description de la réunion (optionnel)</label>
                    <br>
                    <textarea type="text" class="textarea-style" name="Description"></textarea>
                </div>
            </div>
            <div class="content-input spacing-2">
                <h2 class="grid-title">Renseignements sur la salle</h2>
                <div>
                    <label>Equipement audio souhaité</label>
                    <select class="input-style" name="equipaudio">
                        <option value="indifférent">indifférent</option>
                        <option value="oui">oui</option>
                        <option value="non">non</option>
                    </select>
                </div>
                <div>
                    <label>Equipement video souhaité</label>
                    <select class="input-style" name="equipvideo">
                        <option value="indifférent">indifférent</option>
                        <option value="oui">oui</option>
                        <option value="non">non</option>
                    </select>
                </div>
                <div>
                    <label for="nbParticipant">Nombre de participant</label>
                    <input class="input-style" type="number" name="nbParticipant" min="1" required>
                </div>
            </div>
        </div>
        <div class="button-area-meeting">
            <input type="submit" name="envoi" class="button-style-1 clickable" value="Rechercher une salle">
        </div>
    </form>
</div>


<script>
    document.getElementById("day").addEventListener("change", function () {
        const checked = this.checked;
        if (this.checked) {
            document.getElementById("hour").style.visibility = "hidden";
            document.getElementById("duration").style.visibility = "hidden";
            document.getElementById("hdebut").required = false;
            document.getElementById("duree").required = false;
        } else {
            document.getElementById("hour").style.visibility = "visible";
            document.getElementById("duration").style.visibility = "visible";
            document.getElementById("hdebut").required = true;
            document.getElementById("duree").required = true;

        }
    })
</script>
</body>
</html>