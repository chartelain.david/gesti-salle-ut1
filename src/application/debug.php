<?php

$isDebug = $_ENV['DEBUG'];

if ($isDebug === true) {
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);
}

?>