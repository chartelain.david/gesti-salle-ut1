# Gesti-Salle-UT1

This application allows to manage meeting with the Google Calendar API, disallowing users to create meeting that overlaps for the same room.  
The application has been created by @chartelain.david @emiliebutez @ibrahim82370 and @nawfelbrazi during a class project.

# Setup the application

This application is already containerized with Docker.  
All you need to do in order to use it is to install [Docker](https://docs.docker.com/get-docker/) and [Docker compose](https://docs.docker.com/compose/install/) on the system where you want to deploy this application and follow the following steps to configure and run the application.

## Configure the application

All the configuration can be done in the docker-compose.yml file.  
If you don't already know, this file respect the [YAML](https://yaml.org/) format.  
As you can see in this file, there are two containers to setup in order to run the application. 

### db

This container is responsible of the application database. As you can see, the database that we've used is MariaDB. If you want to change the database you may have to change some part of the code as we haven't used an ORM but instead, written the queries via the mysqli library.  

#### Credentials 

To configure the database credentials, you can change the `MARIADB_ROOT_PASSWORD` and `MARIADB_ALLOW_EMPTY_ROOT_PASSWORD` environment variables.

#### Connection

By default the exposed port is 3306. You can change it if you want via the `ports` section.

#### Setup

As you can see, we use a volume and a command to setup the database at the initialization.  
If you want to edit how the database is created you can edit the script located in the `setup/db/` directory.

### web

This service is reponsible to host our web application.  
It uses the Dockerfile to build our container.  

### debug mode

If you need to enable the debug mode (print the errors if any) for some reasons, simply set the `DEBUG` environment variable to `true`.

#### Port

By default the server is exposed on the port 80. However, you can change it via the `ports` section.  
In the configuration, the first port correspond to the host port where to expose the application and the second port is the port exposed by the container.  
So for example if you want to run the application on the port `8000` you can edit the configuration as following :  
```yaml
# [...]

web:
    ports:
        - "8000:80"

# [...]
```

#### Database connection

The database connection is setup with the environments variables specified in the "environment" section.  

As you can see, the database url is setup by default to `db:3306` which can seems weird. However, that's how it must be setup with `docker-compose`. Instead of specifying an url like `localhost` or `0.0.0.0` we specify the targetted container's name.  

## Launch the application

The only step that you need to do after installing `Docker` and `Docker compose` and (optionnaly) configuring the application is to launch the following command in the root directory of the application :
```bash
docker-compose up
```

That's it, you're done !  
The server is now running on the port you specified earlier (80 by default). To access it, simply go on your favorite navigator and on the `localhost:[specified port]` address.  
With the default configuration, simply go on that link : [Application link](http://localhost:80).


## Default user

The default user used to access the application is the following :

```yaml
email: gesti.salle.ut1@gmail.com
password: G€sti-S@lle-UT1!
```

You can use it to access the application the first time and start adding some users, rooms and so on to it.

# TODO

Here are the things that have not been implemented or that should be corrected :
 - Rewrite some SQL queries as prepared statements.
 - Maybe switch to an ORM to allow better maintenability.
 - Use transactions everywhere, especially where we have to do multiple things to complete a database operation.
 - Hash passwords for security !
 - Allow users to create meeting that last a full day.
 - Directly validate meeting update if the already choosen class is still free for the specified hours.