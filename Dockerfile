FROM php:8.0-apache


WORKDIR /var/www/html
RUN apt-get update && apt-get install mysql-common unzip -y
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
COPY src/ .
COPY setup/ setup/
RUN chmod +x setup/web/composer-setup.sh && ./setup/web/composer-setup.sh && php composer.phar update
RUN chown www-data:www-data ../html
