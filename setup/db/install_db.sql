-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : dim. 20 juin 2021 à 12:38
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gesti-salle-ut1`
--
CREATE DATABASE IF NOT EXISTS `gesti-salle-ut1` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `gesti-salle-ut1`;

-- --------------------------------------------------------

--
-- Structure de la table `participant`
--

CREATE TABLE IF NOT EXISTS `participant` (
  `emailparticipant` varchar(50) NOT NULL,
  `id_reunion` int(11) NOT NULL,
  PRIMARY KEY (`emailparticipant`,`id_reunion`),
  KEY `id_reunion` (`id_reunion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `reunion`
--

CREATE TABLE IF NOT EXISTS `reunion` (
  `id_reunion` int(11) NOT NULL AUTO_INCREMENT,
  `reuniondate` date NOT NULL,
  `reunionnom` varchar(50) DEFAULT NULL,
  `heuredebut` datetime DEFAULT NULL,
  `reunionduree` time DEFAULT NULL,
  `recurrence` text DEFAULT NULL,
  `freqreccu` int(11) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `nbparticipant` int(11) DEFAULT NULL,
  `num_salle` varchar(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `eventId` tinytext DEFAULT NULL,
  PRIMARY KEY (`id_reunion`),
  KEY `num_salle` (`num_salle`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

CREATE TABLE IF NOT EXISTS `salle` (
  `num_salle` varchar(11) NOT NULL,
  `capacite` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  `equipementvisuel` varchar(50) DEFAULT NULL,
  `equipementaudio` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`num_salle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tempsReservation`
--

CREATE TABLE IF NOT EXISTS `tempsReservation` (
  `num_salle` varchar(11) CHARACTER SET utf8 NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `until` datetime NOT NULL,
  PRIMARY KEY (`num_salle`),
  KEY `id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `statut` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `participant`
--
ALTER TABLE `participant`
  ADD CONSTRAINT `participant_ibfk_1` FOREIGN KEY (`id_reunion`) REFERENCES `reunion` (`id_reunion`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reunion`
--
ALTER TABLE `reunion`
  ADD CONSTRAINT `reunion_ibfk_1` FOREIGN KEY (`num_salle`) REFERENCES `salle` (`num_salle`) ON DELETE CASCADE,
  ADD CONSTRAINT `reunion_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tempsReservation`
--
ALTER TABLE `tempsReservation`
  ADD CONSTRAINT `tempsReservation_ibfk_1` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE CASCADE,
  ADD CONSTRAINT `tempsReservation_ibfk_2` FOREIGN KEY (`num_salle`) REFERENCES `salle` (`num_salle`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


INSERT INTO utilisateur (`email`, `statut`, `password`) VALUES ('gesti.salle.ut1@gmail.com', 'superutilisateur', 'G€sti-S@lle-UT1!');